package client

import (
	"fmt"
	"github.com/djumanoff/amqp"
	"lesson-14-djumanoff/client/config"
	"lesson-14-djumanoff/client/rpc/rabbit"
)

func main() {
	fmt.Println("Client start")
	configs, err := config.GetConfigs()
	if err != nil {
		panic(err)
	}

	rabbitMQSession, err := rabbit.InitRabbitMQConnection(configs)
	if err != nil {
		panic(err)
	}
	defer (*rabbitMQSession).Close()

	rabbitMQClient, err := rabbit.GetRabbitMQClient(rabbitMQSession)
	if err != nil {
		panic(err)
	}

	reply, err := (*rabbitMQClient).Call(
		"request.get.test", amqp.Message{
			Body: []byte("ping 1"),
		},
	)
	if err != nil {
		panic(err)
	}
	fmt.Println(reply)
	fmt.Println("Client end")
}
