package config

import (
	"encoding/json"
	"os"
)

// Config structures & vars
type Configs struct {
	Rabbit  RabbitConfig  `json:"rabbit"`
}

type RabbitConfig struct {
	Host        string `json:"host"`
	VirtualHost string `json:"virtual_host"`
	Port        int    `json:"port"`
	User        string `json:"user"`
	Password    string `json:"password"`
	LogLevel    uint8  `json:"log_level"`
}

// Get configs
func GetConfigs() (*Configs, error) {

	var filePath string
	var configs Configs

	currentDir, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	if last := len(currentDir) - 1; currentDir[last] == '/' {
		currentDir = currentDir[:last]
	}
	if os.Getenv("config") != "" {
		filePath = currentDir + "/src/config/" + os.Getenv("config")
	} else {
		filePath = currentDir + "/src/config/config.json"
	}

	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configs)
	if err != nil {
		return nil, err
	}

	return &configs, nil
}
