package server

import (
	"fmt"
	"github.com/djumanoff/amqp"
	"gitlab.com/diassyes/lesson-14-djumanoff/server/config"
	"gitlab.com/diassyes/lesson-14-djumanoff/server/rpc/rabbit"
)

func main() {
	fmt.Println("Server start")
	configs, err := config.GetConfigs()
	if err != nil {
		panic(err)
	}

	rabbitMQSession, err := rabbit.InitRabbitMQConnection(configs)
	if err != nil {
		panic(err)
	}
	defer (*rabbitMQSession).Close()

	rabbitMQServer, err := rabbit.GetRabbitMQServer(rabbitMQSession)
	if err != nil {
		panic(err)
	}

	_ = (*rabbitMQServer).Endpoint(
		"request.get.test",
		func(d amqp.Message) *amqp.Message {
			fmt.Println("handler called")
			return &amqp.Message{
				Body: []byte("test"),
			}
		},
	)

	if err := (*rabbitMQServer).Start(); err != nil {
		panic(err)
	}

	fmt.Println("Server end")
}
