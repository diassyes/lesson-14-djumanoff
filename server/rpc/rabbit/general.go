package rabbit

import (
	"github.com/djumanoff/amqp"
	"lesson-14-djumanoff/server/config"
)

// Init RabbitMQ connection
func InitRabbitMQConnection(configs *config.Configs) (*amqp.Session, error) {
	amqpConfig := amqp.Config{
		Host:        configs.Rabbit.Host,
		VirtualHost: configs.Rabbit.VirtualHost,
		User:        configs.Rabbit.User,
		Password:    configs.Rabbit.Password,
		Port:        configs.Rabbit.Port,
		LogLevel:    configs.Rabbit.LogLevel,
	}
	amqpSession := amqp.NewSession(amqpConfig)
	err := amqpSession.Connect()
	if err != nil {
		return nil, err
	}
	return &amqpSession, nil
}

// Get RabbitMQ server
func GetRabbitMQServer(sess *amqp.Session) (*amqp.Server, error) {
	serverConfig := amqp.ServerConfig{
	}
	amqpServer, err := (*sess).Server(serverConfig)
	if err != nil {
		return nil, err
	}
	return &amqpServer, nil
}
